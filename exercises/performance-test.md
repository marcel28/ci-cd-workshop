# Performance testing

Performance testing is validating whether the application is performing.

## Goals

Performance testing has four purposes:

- Find a breaking point of a application
- How does the application  behave under stress
- What kind of sizing does the application need
- How does the application under stress and normal load

## Approach

Performance testing of this application is done through a framework called [artillery](https://artillery.io/). The 
definition of the performance test are defined in the directory [performance](../performance). This definition does a 
REST call on the endpoint.

Within the [package.json](../package.json) file there is a performance test job defined. This will call the artillery 
framework.
 
## Exercise

In this exercise a performance test job and performance stage are created that validate the performance of the endpoint

### Step 1: Performance test

Create a performance job and a performance stage in the continuous delivery pipeline. Add the following parts to 
the [.gitlab-ci.yml](../.gitlab-ci.yml) 

```yaml
stages:
# Disable
#  - sample
  - qa
  - build
  - test
  - uat
  - deploy
  - smoke
  - performance

# The performance test job
performance:
  stage: performance
  script:
  - npm run performance
```

The stage **performance** is an new logical divider of steps within the continuous delivery pipeline. The job **performance** is 
actual command that starts the performance test.
